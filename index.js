// Soal 1
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

function jumlah_kata(str) {
  str = str.replace(/(^\s*)|(\s*$)/gi, "");
  str = str.replace(/[ ]{2,}/gi, " ");
  str = str.replace(/\n /, "\n");
  return str.split(" ").length;
}
console.info(jumlah_kata(kalimat_1));
console.info(jumlah_kata(kalimat_2));

// Soal 2

  var someDate = new Date(`${tahun}-${tempBulan}-${tanggal}`);
  someDate.setDate(someDate.getDate() + 1);
  switch (someDate.getMonth()) {
    case 1: {
      console.log(someDate.getDate() + " Januari " + someDate.getFullYear());
      break;
    }
    case 2: {
      console.log(someDate.getDate() + " Febuari " + someDate.getFullYear());
      break;
    }
    case 3: {
      console.log(someDate.getDate() + " Maret" + someDate.getFullYear());
      break;
    }
    case 4: {
      console.log(someDate.getDate() + " April " + someDate.getFullYear());
      break;
    }
    case 5: {
      console.log(someDate.getDate() + " Mei " + someDate.getFullYear());
      break;
    }
    case 7: {
      console.log(someDate.getDate() + " Juni " + someDate.getFullYear());
      break;
    }
    case 6: {
      console.log(someDate.getDate() + " Juli " + someDate.getFullYear());
      break;
    }
    case 8: {
      console.log(someDate.getDate() + " Agustus " + someDate.getFullYear());
      break;
    }
    case 9: {
      console.log(someDate.getDate() + " September " + someDate.getFullYear());
      break;
    }
    case 10: {
      console.log(someDate.getDate() + " Oktober " + someDate.getFullYear());
      break;
    }
    case 11: {
      console.log(someDate.getDate() + " November " + someDate.getFullYear());
      break;
    }
    case 12: {
      console.log(someDate.getDate() + " Desember " + someDate.getFullYear());
      break;
    }
    default: {
      console.log("Harap Input Bulan");
    }
  }
}


