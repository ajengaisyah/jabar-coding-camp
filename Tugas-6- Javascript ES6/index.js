// soal 1

const hitungPersegi = (panjang, lebar) => {
  let luas = panjang * lebar;
  let keliling = 2 * (panjang + lebar);
  console.log("luas : " + luas);
  console.log("keliling : " + keliling);
};

hitungPersegi(5, 4);

// soal 2

const newFunction = function literal(firstName, lastName) {
  return {
    fullName: function () {
      console.log(firstName + " " + lastName);
    },
  };
};

newFunction("William", "Imoh").fullName();


// soal 3 


const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

// soal 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combinedArray = [...west, ...east];
console.log(combinedArray);

// soal 5

const planet = "earth";
const view = "glass";
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,  ${planet}`;

console.info(after);
