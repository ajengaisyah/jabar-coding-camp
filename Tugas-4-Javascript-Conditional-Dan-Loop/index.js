// Soal 1

var nilai = 75;

if (nilai >= 85) {
  console.info("A");
} else if (nilai >= 75 && nilai < 85) {
  console.info("B");
} else if (nilai >= 65 && nilai < 75) {
  console.info("C");
} else if (nilai >= 55 && nilai < 65) {
  console.info("D");
} else if (nilai < 55) {
  console.info("E");
}

// Soal 2

var tanggal = 12;
var bulan = 2;
var tahun = 2000;

switch (bulan) {
  case 1: {
    console.log(tanggal + " Januari " + tahun);
    break;
  }
  case 2: {
    console.log(tanggal + " Febuari " + tahun);
    break;
  }
  case 3: {
    console.log(tanggal + " Maret" + tahun);
    break;
  }
  case 4: {
    console.log(tanggal + " April " + tahun);
    break;
  }
  case 5: {
    console.log(tanggal + " Mei " + tahun);
    break;
  }
  case 7: {
    console.log(tanggal + " Juni " + tahun);
    break;
  }
  case 6: {
    console.log(tanggal + " Juli " + tahun);
    break;
  }
  case 8: {
    console.log(tanggal + " Agustus " + tahun);
    break;
  }
  case 9: {
    console.log(tanggal + " September " + tahun);
    break;
  }
  case 10: {
    console.log(tanggal + " Oktober " + tahun);
    break;
  }
  case 11: {
    console.log(tanggal + " November " + tahun);
    break;
  }
  case 12: {
    console.log(tanggal + " Desember " + tahun);
    break;
  }
  default: {
    console.log("Harap Input Bulan");
  }
}

// soal 3

let n = 3;
let hasil = "";
for (let i = 0; i < n; i++) {
  for (let j = 0; j <= i; j++) {
    hasil += "# ";
  }
  hasil += "\n";
}
console.info(hasil);

// soal 4

let m = 3,
  len = 1;
for (let idx = 1; idx <= m; ++idx, len++) {
  if (len - 1 === 0) {
    console.log(`${idx} - I Love Programming`);
  } else if (len - 1 === 1) {
    console.log(`${idx} - I Love Javascript`);
  } else if (len - 1 === 2) {
    console.log(`${idx} - I Love VueJS`);
  }
  if (idx % 3 === 0) {
    len = 0;
    console.log("===");
  }
}
