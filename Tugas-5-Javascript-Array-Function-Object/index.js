// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var dataFix = daftarHewan.sort();

for (let index = 0; index < dataFix.length; index++) {
  console.info(dataFix[index]);
}

// soal 2

function introduce(data) {
  return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3

function hitung_huruf_vokal(str) {
  var vokal = "aeiouAEIOU";
  var count = 0;

  for (var x = 0; x < str.length; x++) {
    if (vokal.indexOf(str[x]) !== -1) {
      count++;
    }
  }
  return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);

// soal 4

function hitung(data) {
  return parseInt(data * 2 - 2);
}
console.info(hitung(0));
console.info(hitung(1));
console.info(hitung(2));
console.info(hitung(3));
console.info(hitung(5));
